<?php

namespace App\Services;

use App\Repositories\NewsletterRepository;
use App\Models\Newsletter;

class NewsletterService
{

    /**
     * @var NewsletterRepository
     */
    public $newsletterRepository;

    /**
     * @param NewsletterRepository $newsletterRepository
     */
    public function __construct(NewsletterRepository $newsletterRepository){
        $this->newsletterRepository = $newsletterRepository;
    }

    /**
     * @param array $data
     * @return Newsletter
     */
    public function saveNewsletter(array $data): Newsletter
    {
        return $this->newsletterRepository->store($data);
    }
}
