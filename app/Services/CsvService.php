<?php

namespace App\Services;

use League\Csv\CannotInsertRecord;
use League\Csv\Writer;
use SplTempFileObject;
use League\Csv\Reader;
use League\Csv\Statement;
class CsvService
{

    /**
     * @var
     */
    private $csv;

    /**
     * @param string $file
     * @param array $head
     * @param array $body
     */
    public function writeToFile(string $file, array $head, array $body)
    {
        try{
            $this->csv = Reader::createFromPath($file, 'r')->setHeaderOffset(0);
            $oldRecords = $this->getOldRecords($this->csv, $head);
            $this->csv = Writer::createFromPath($file, 'w');
            $this->insertHeadAndBody($head, $body, $oldRecords);
        }catch(\Exception $e){
            $this->csv = Writer::createFromPath($file, 'w');
            $this->insertHeadAndBody($head, $body);
        }
    }

    /**
     * @param $csv
     * @param array $head
     * @return array
     */
    private function getOldRecords($csv, array $head): array
    {
        $oldRecords = [];
        foreach ($csv as $record) {
            foreach($head as $item){
                $oldRecords[] = $record[$item];
            }
        }
        return $oldRecords;
    }

    /**
     * @param $head
     * @param $body
     * @param null $old
     */
    private function insertHeadAndBody($head, $body, $old = null)
    {
        $this->csv->insertOne($head);
        if($old) $this->csv->insertAll($this->turnModelToBody($old));
        $this->csv->insertAll($body);
    }

    /**
     * @param array $data
     * @return array
     */
    public function turnModelToBody(array $data): array
    {
        $body = [];
        foreach($data as $property){
            $body[] = [$property];
        }
        return $body;
    }

}
