<?php

Namespace App\Repositories;

use App\Models\Newsletter;

class NewsletterRepository
{
    /**
     * @param array $data
     * @return Newsletter
     */
    public function store(array $data): Newsletter
    {
        return Newsletter::create($data);
    }
}
