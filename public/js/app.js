let xhr = new XMLHttpRequest();
const loading = document.getElementById('loading');
const result = document.getElementById('result');

document.querySelector('#newsletter-form').addEventListener('submit', (e) => {
    e.preventDefault();
    clearResult();
    displayLoading();
    postNewsletter();
});

xhr.onreadystatechange = function() {
    if (xhr.readyState === XMLHttpRequest.DONE) {
        let response = JSON.parse(xhr.response);
        responseHasErrors(response) ? displayErrorResult(response.errors) : displaySuccessResult(response.success);
        hideLoading();
    }
}

function postNewsletter(){
    let csrfToken = document.getElementById('csrf-token').value;
    let route = document.getElementById('route').value;
    xhr.open("POST", route , true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.setRequestHeader('x-csrf-token', csrfToken);
    xhr.send(JSON.stringify({
        email: document.getElementById('email-input').value
    }));
}

function displayErrorResult(errors){
    let error = '<ul>';
    setResultsColor('red');
    Object.keys(errors).forEach(function(key) {
        error = error + '<li>' + errors[key] + '</li>';
    });
    result.innerHTML = error + '</ul>';
}

function displaySuccessResult(success){
    setResultsColor('green');
    result.innerHTML = '<p>' + success + '</p>';
}

function responseHasErrors(response){
    return !!response.errors;
}

function setResultsColor(color){
    result.style.color = color;
}

function clearResult(){
    result.innerHTML = '';
}

function displayLoading(){
    loading.style.display = 'block';
}

function hideLoading(){
    loading.style.display = 'none';
}

