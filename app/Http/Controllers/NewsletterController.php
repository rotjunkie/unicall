<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreNewsletterRequest;
use App\Models\Newsletter;
use App\Services\NewsletterService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Services\CsvService;

class NewsletterController extends Controller
{
    const SUCCESS  = 'success';
    const ERRORS   = 'errors';
    const ERROR    = 'error';
    const EMAIL    = 'email';
    const CSV      = 'subscribers.csv';
    const ERROR_MESSAGE = 'There was an error saving your subscription.';
    const SUCCESS_MESSAGE = "You've successfully subscribed to our newsletter!";

    /**
     * @var NewsletterService
     */
    private $newsletterService;

    /**
     * @var CsvService
     */
    private $csvService;

    /**
     * @var array
     */
    public $csvHead;

    /**
     * @param NewsletterService $newsletterService
     * @param CsvService $csvService
     * @param array $csvHead
     */
    public function __construct(NewsletterService $newsletterService, CsvService $csvService, array $csvHead = [])
    {
        $this->csvService        = $csvService;
        $this->newsletterService = $newsletterService;
        $this->csvHead[] = NewsletterController::EMAIL;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * @param StoreNewsletterRequest $request
     * @return JsonResponse
     */
    public function store(StoreNewsletterRequest $request): JsonResponse
    {
        try{
            $data = $request->getData();
            $this->newsletterService->saveNewsletter($data);
            $this->csvService->writeToFile(NewsletterController::CSV, $this->csvHead, $this->csvService->turnModelToBody($data));
        } catch(\Exception $e){
            return response()->json([ NewsletterController::ERRORS => [ NewsletterController::ERROR => __(NewsletterController::ERROR_MESSAGE) ] ]);
        }
        return response()->json([ NewsletterController::SUCCESS => __(NewsletterController::SUCCESS_MESSAGE)]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function show(Newsletter $newsletter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function edit(Newsletter $newsletter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Newsletter $newsletter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Newsletter  $newsletter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Newsletter $newsletter)
    {
        //
    }
}
